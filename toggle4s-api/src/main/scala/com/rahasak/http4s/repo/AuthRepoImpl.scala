package com.rahasak.http4s.repo

import cats.effect.IO
import com.rahasak.http4s.config.LdapConfig

import java.util.Properties
import javax.naming.Context
import javax.naming.directory.InitialDirContext
import scala.util.{Failure, Success, Try}

class AuthRepoImpl(ldapConfig: LdapConfig) extends AuthRepo {
  override def getAuthUser(username: String, password: String): IO[Option[AuthUser]] = {
    IO {
      // authenticate user from ldap
      Try {
        val props = new Properties()
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory")
        props.put(Context.PROVIDER_URL, ldapConfig.host)
        props.put(Context.SECURITY_PRINCIPAL, s"uid=$username,dc=${ldapConfig.dc1},dc=${ldapConfig.dc2}")
        props.put(Context.SECURITY_CREDENTIALS, password)

        new InitialDirContext(props)
      } match {
        case Success(_) => Option(AuthUser(1, username))
        case Failure(_) => None
      }
    }
  }
}