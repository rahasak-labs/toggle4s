package com.rahasak.http4s.repo

import cats.effect.IO
import cats.effect.kernel.Resource
import com.rahasak.http4s.util.DateUtil
import skunk.codec.all._
import skunk.implicits._
import skunk.{Session, _}

/**
  * implement toggles repository functions
  *
  * @param pool skunk postgres session
  */
class ToggleRepoImpl(pool: Resource[IO, Resource[IO, Session[IO]]]) extends ToggleRepo {

  /**
    * decode postgres data types into toggles case class
    * convert postgres timestamp into util.date
    */
  val toggleDecoder: Decoder[Toggle] =
    (uuid ~ varchar(50) ~ text ~ text ~ timestamp).map {
      case i ~ n ~ s ~ v ~ t => Toggle(Option(i), n, s, v, DateUtil.toDate(t))
    }

  /**
    * encode toggles case class data into postgres data types
    * convert toggle case class util.Date into postgres timestamp
    */
  val toggleEncoder = (uuid.opt ~ varchar(50) ~ text ~ text ~ timestamp)
    .values.contramap((t: Toggle) => t.id ~ t.name ~ t.service ~ t.value ~ DateUtil.toLocalDateTime(t.timestamp))

  /**
    * create toggles
    *
    * @param toggle toggle object
    * @return IO[Unit]
    */
  override def createToggle(toggle: Toggle): IO[Unit] = {
    // define query as skunk command
    // use toggle encoder to map toggle case class fields into postgres types
    val q: Command[Toggle] =
    sql"""
         INSERT INTO toggles(id, name, service, value, timestamp)
         VALUES $toggleEncoder
       """
      .command

    // execute command directly since nothing returned
    pool.use(_.use(s => s.prepare(q).use(_.execute(toggle).void)))
  }

  /**
    * update toggle with given name
    *
    * @param toggle updating toggle
    * @return IO[Unit]
    */
  override def updateToggle(toggle: Toggle): IO[Unit] = {
    // define query as skunk command
    // query parameters passed as postgres types/codecs
    val q: Command[String ~ String] =
    sql"""
        UPDATE toggles
        SET value = $text
        WHERE name = $text
      """
      .command

    // execute command directly since nothing returned
    pool.use(_.use(s => s.prepare(q).use(_.execute(toggle.value ~ toggle.name)).void))
  }

  /**
    * delete toggle with given name
    *
    * @param name toggle name
    * @return IO[Unit]
    */
  override def deleteToggle(name: String): IO[Unit] = {
    // define query as skunk command
    // query parameters passed as postgres types/codecs
    val q: Command[String] =
    sql"""
        DELETE FROM toggles
        WHERE name = $text
      """
      .command

    // execute command directly since nothing returned
    pool.use(_.use(s => s.prepare(q).use(_.execute(name)).void))
  }

  /**
    * get toggle with given name
    *
    * @param name toggle name
    * @return IO[Toggle]
    */
  override def getToggle(service: String, name: String): IO[Option[Toggle]] = {
    val q: Query[String, Toggle] = {
      // define query as skunk query
      // used toggle decoder to decode postgres types into toggle case class
      sql"""
        SELECT id, name, service, value, timestamp
        FROM toggles
        WHERE name = $text
        LIMIT 1
      """
        .query(toggleDecoder)
    }

    // create prepared statement with binding query parameters
    // execute query as fs2 stream
    // chunk size defines number of rows need to be fetched at once
    // getting a single row or throw error if not exists
    pool.use(_.use { s =>
      s.prepare(q).use { ps =>
        ps.stream(name, 64)
          //.take(1)
          .compile
          .last
      }
    })
  }

  /**
    * get toggles with matching service name
    *
    * @param service service name
    * @return IO[List[Toggles]]
    */
  override def getServiceToggles(service: String): IO[List[Toggle]] = {
    // define query as skunk query
    // used toggle decoder to decode postgres types into toggle case class
    val q: Query[String, Toggle] =
    sql"""
        SELECT id, name, service, value, timestamp
        FROM toggles
        WHERE service = $text
        LIMIT 1
      """
      .query(toggleDecoder)

    // create prepared statement with binding query parameters
    // execute query as fs2 stream
    // chunk size defines number of rows need to be fetched at once
    // getting list rows
    pool.use(_.use { s =>
      s.prepare(q).use { ps =>
        ps.stream(service, 32)
          .compile
          .toList
      }
    })
  }

  /**
    * get all toggles
    *
    * @return IO[List[Toggles]]
    */
  override def getAllToggles: IO[List[Toggle]] = {
    // define query as skunk query
    // used toggle decoder to decode postgres types into toggle case class
    val q: Query[Void, Toggle] =
    sql"""
        SELECT id, name, service, value, timestamp
        FROM toggles
      """
      .query(toggleDecoder)

    // execute query directly with session
    pool.use(_.use(s => s.execute(q)))
  }

}
