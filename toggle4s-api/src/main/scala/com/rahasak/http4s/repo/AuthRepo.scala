package com.rahasak.http4s.repo

import cats.effect.IO

case class AuthUser(id: Long, username: String)

trait AuthRepo {
  def getAuthUser(username: String, password: String): IO[Option[AuthUser]]
}
