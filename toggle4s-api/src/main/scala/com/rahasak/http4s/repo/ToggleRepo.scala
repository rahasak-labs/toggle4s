package com.rahasak.http4s.repo

import cats.effect.IO

import java.util.{Date, UUID}

case class Toggle(id: Option[UUID] = Option(UUID.randomUUID()), service: String, name: String, value: String, timestamp: Date = new Date())

/**
  * define curd operations of toggles table
  * return values wrapped with IO monad
  */
trait ToggleRepo {
  def createToggle(toggle: Toggle): IO[Unit]

  def updateToggle(toggle: Toggle): IO[Unit]

  def deleteToggle(name: String): IO[Unit]

  def getToggle(service: String, name: String): IO[Option[Toggle]]

  def getServiceToggles(service: String): IO[List[Toggle]]

  def getAllToggles: IO[List[Toggle]]
}
