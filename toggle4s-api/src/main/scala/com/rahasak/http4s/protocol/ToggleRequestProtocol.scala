package com.rahasak.http4s.protocol

case class ToggleRequest(service: String, name: String, value: String)

case class ToggleReply(id: Option[String] = None, service: String, name: String, value: String, timestamp: String)

case class ServiceReply(name: String, toggles: List[ToggleReply])

