package com.rahasak.http4s.protocol

case class AuthRequest(username: String, password: String)
