package com.rahasak.http4s

import cats.data.Kleisli
import cats.effect.kernel.Resource
import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._
import com.rahasak.http4s.auth.TokenAuth
import com.rahasak.http4s.config.{Config, PostgresConfig}
import com.rahasak.http4s.http.{AuthRoute, ToggleRoute}
import com.rahasak.http4s.repo.{AuthRepoImpl, ToggleRepoImpl}
import fs2.Stream
import natchez.Trace.Implicits.noop
import org.http4s.blaze.server.BlazeServerBuilder
import org.http4s.implicits._
import org.http4s.server.Router
import org.http4s.{Request, Response}
import skunk.Session

object Main extends IOApp {

  def skunkSession(postgresConfig: PostgresConfig): Resource[IO, Session[IO]] = {
    val session: Resource[IO, Session[IO]] = {
      Session.single(
        host = postgresConfig.host,
        port = postgresConfig.port,
        user = postgresConfig.user,
        database = postgresConfig.database,
        password = Some(postgresConfig.password)
      )
    }

    session
  }

  def skunkPool(postgresConfig: PostgresConfig): Resource[IO, Resource[IO, Session[IO]]] = {
    val pool = {
      Session.pooled[IO](
        host = postgresConfig.host,
        port = postgresConfig.port,
        user = postgresConfig.user,
        database = postgresConfig.database,
        password = Some(postgresConfig.password),
        max = 10
      )
    }

    pool
  }

  def makeRouter(config: Config, pool: Resource[IO, Resource[IO, Session[IO]]]): Kleisli[IO, Request[IO], Response[IO]] = {
    // auth route without authentication
    // token route with authentication
    val authRoutes = AuthRoute.routes(new AuthRepoImpl(config.ldapConfig))
    val authedTokenRoutes = TokenAuth.authMiddleware.apply(ToggleRoute.routes(new ToggleRepoImpl(pool)))
    val routes = authRoutes <+> authedTokenRoutes
    Router[IO](
      "/api/v1" -> routes
    ).orNotFound
  }

  def serveStream(config: Config, pool: Resource[IO, Resource[IO, Session[IO]]]): Stream[IO, ExitCode] = {
    BlazeServerBuilder[IO]
      .bindHttp(config.serverConfig.port, config.serverConfig.host)
      .withHttpApp(makeRouter(config, pool))
      .serve
  }

  override def run(args: List[String]): IO[ExitCode] = {
    val stream = for {
      config <- Stream.eval(Config.load())
      exitCode <- serveStream(config, skunkPool(config.postgresConfig))
    } yield exitCode

    stream.compile.drain.as(ExitCode.Success)
  }

}
