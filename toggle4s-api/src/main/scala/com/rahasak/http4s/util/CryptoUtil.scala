package com.rahasak.http4s.util

import java.util.Base64

object CryptoUtil {

  def sign(payload: String): String = {
    // TODO enabled sign

    // signature as Base64 encoded string
    Base64.getEncoder.encodeToString(payload.getBytes)
      .replaceAll("\n", "")
      .replaceAll("\r", "")
  }

  def verifySignature(payload: String, signedPayload: String): Boolean = {
    // TODO enable verify signature
    payload == signedPayload
  }

  def encodeBase64(payload: String): String = {
    Base64.getEncoder.encodeToString(payload.getBytes("UTF-8"))
      .replaceAll("\n", "")
      .replaceAll("\r", "")
  }

  def decodeBase64(payload: String) = {
    new String(Base64.getDecoder.decode(payload), "UTF-8")
  }

}