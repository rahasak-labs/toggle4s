package com.rahasak.http4s.util

import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.{Date, TimeZone}

object DateUtil {

  val COLOMBO_TIME_ZONE = TimeZone.getTimeZone("Asia/Colombo")
  val UTC_TIME_ZONE = TimeZone.getTimeZone("UTC")
  val TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS"
  val TIMESTAMP_FORMAT_E = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
  val DATE_FORMAT = "yyyy-MM-dd"

  /**
    * Format date to string date with given format
    * @param date date
    * @param format date format
    * @param timeZone timezone
    * @return
    */
  def toString(date: Option[Date], format: String, timeZone: TimeZone): Option[String] = {
    date match {
      case Some(d) =>
        val sdf = new SimpleDateFormat(format)
        sdf.setTimeZone(timeZone)
        Option(sdf.format(d))
      case None =>
        None
    }
  }

  /**
    * toggles timestamp filed in database stored as java.time.LocalDateTime,
    * toggles case class timestamp field defined as java.util.Data
    * so need function to convert java.util.Date into java.time.LocalDateTime when saving data
    * @param date util date
    * @return local date
    */
  def toLocalDateTime(date: Date) = {
    import java.time.{Instant, LocalDateTime, ZoneOffset}
    val instant = Instant.ofEpochMilli(date.getTime)
    val ldt = LocalDateTime.ofInstant(instant, ZoneOffset.UTC)
    ldt
  }

  /**
    * toggles timestamp filed in database stored as java.time.LocalDateTime,
    * toggles case class timestamp field defined as java.util.Data
    * so need function to convert java.time.LocalDateTime into java.util.Date when quering the data
    * @param ldt local date
    * @return util date
    */
  def toDate(ldt: LocalDateTime) = {
    import java.time.ZoneOffset
    val instant = ldt.toInstant(ZoneOffset.UTC)
    val date = Date.from(instant)
    date
  }

  def timestamp(): Long = {
    System.currentTimeMillis() / 1000
  }
}
