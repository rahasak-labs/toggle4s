package com.rahasak.http4s.auth

import cats.data._
import cats.effect._
import com.rahasak.http4s.util.CryptoUtil
import io.circe.generic.auto._
import io.circe.parser.decode
import org.http4s._
import org.http4s.server.AuthMiddleware
import org.typelevel.ci.CIString

import scala.util.Try

object TokenAuth {

  sealed trait Error

  case object AnyError extends Error

  object AuthToken {
    def apply(id: String, roles: String, groups: String, issueTime: Long, ttl: Long): AuthToken = {
      val digsig = CryptoUtil.sign(s"$id$roles$groups$issueTime$ttl".toLowerCase)
      AuthToken(id, roles, groups, issueTime, ttl, digsig)
    }
  }

  case class AuthToken(id: String, roles: String, groups: String, issueTime: Long, ttl: Long, digsig: String)

  def getAuthTokenFromHeader(authHeader: String): Option[AuthToken] = {
    println(s"validate bearer token $authHeader")
    Try {
      authHeader.split(" ")
        .lastOption
        .map(t => CryptoUtil.decodeBase64(t.trim))
        .map(t => decode[AuthToken](t))
        .flatMap(_.toOption)
    }.toOption.flatten
  }

  def authToken: Kleisli[IO, Request[IO], Either[Error, AuthToken]] = Kleisli { request: Request[IO] =>
    // get Authorization header
    val header: Option[NonEmptyList[Header.Raw]] = request.headers.get(CIString("Authorization"))
    header
      .flatMap(h => getAuthTokenFromHeader(h.head.value))
      .map(t => IO.pure(Right(t)))
      .getOrElse(IO.pure(Left(AnyError)))
  }

  def onAuthFailure: AuthedRoutes[Error, IO] = Kleisli { req: AuthedRequest[IO, Error] =>
    // for any requests' auth failure we return 401
    req.req match {
      case _ =>
        OptionT.pure[IO](
          Response[IO](
            status = Status.Unauthorized
          )
        )
    }
  }

  // auth middleware
  def authMiddleware: AuthMiddleware[IO, AuthToken] = AuthMiddleware(authToken, onAuthFailure)

}
