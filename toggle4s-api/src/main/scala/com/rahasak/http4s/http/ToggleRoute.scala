package com.rahasak.http4s.http

import cats.effect.IO
import com.rahasak.http4s.auth.TokenAuth.AuthToken
import com.rahasak.http4s.protocol.{ToggleReply, ToggleRequest}
import com.rahasak.http4s.repo.{Toggle, ToggleRepo}
import com.rahasak.http4s.util.DateUtil
import io.circe.generic.auto._
import org.http4s._
import org.http4s.circe.CirceEntityCodec._
import org.http4s.dsl.Http4sDsl


object ToggleRoute {
  /**
    * define following APIS
    *   1. create toggle — POST api/v1/toggles/{service}
    *      2. update toggle — PUT api/v1/toggles/{service}/{name}
    *      3. get toggle — GET api/v1/toggles/{service}/{name}
    *      4. get service toggles — GET api/v1/toggles/{service}
    *      5. get all toggles — GET api/v1/toggles
    */
  def routes(toggleRepo: ToggleRepo): AuthedRoutes[AuthToken, IO] = {
    val dsl = new Http4sDsl[IO] {}
    import dsl._

    AuthedRoutes.of {
      case authReq@POST -> Root / "toggles" / service as _ =>
        authReq.req.decode[ToggleRequest] { t =>
          toggleRepo.createToggle(Toggle(name = t.name, service = service, value = t.value))
            .flatMap(_ => Created())
            .handleErrorWith(e => BadRequest(s"fail create toggle, ${e.getMessage}"))
        }.handleErrorWith(e => InternalServerError(e.getMessage))
      case authReq@PUT -> Root / "toggles" / service / name as _ =>
        authReq.req.decode[ToggleRequest] { t =>
          toggleRepo.updateToggle(Toggle(name = name, service = service, value = t.value))
            .flatMap(_ => Accepted())
            .handleErrorWith(e => BadRequest(s"fail update toggle, ${e.getMessage}"))
        }.handleErrorWith(e => InternalServerError(e.getMessage))
      case _@GET -> Root / "toggles" / service / name as _ =>
        toggleRepo.getToggle(service, name)
          .flatMap {
            case Some(t) =>
              Ok(ToggleReply(t.id.map(_.toString), t.service, t.name, t.value, DateUtil.toString(Option(t.timestamp), DateUtil.TIMESTAMP_FORMAT, DateUtil.COLOMBO_TIME_ZONE).getOrElse("")))
            case None =>
              NotFound()
          }
          .handleErrorWith(e => InternalServerError(e.getMessage))
      case _@GET -> Root / "toggles" / service as _ =>
        toggleRepo.getServiceToggles(s"toggles/$service/")
          .flatMap { toggles =>
            Ok(toggles.map(t => ToggleReply(t.id.map(_.toString), t.service, t.name, t.value,
              DateUtil.toString(Option(t.timestamp), DateUtil.TIMESTAMP_FORMAT, DateUtil.COLOMBO_TIME_ZONE).getOrElse(""))))
          }
          .handleErrorWith(e => InternalServerError(e.getMessage))
      case _@GET -> Root / "toggles" as _ =>
        toggleRepo.getAllToggles
          .flatMap { toggles =>
            Ok(toggles.map(t => ToggleReply(t.id.map(_.toString), t.service, t.name, t.value,
              DateUtil.toString(Option(t.timestamp), DateUtil.TIMESTAMP_FORMAT, DateUtil.COLOMBO_TIME_ZONE).getOrElse(""))))
          }
          .handleErrorWith(e => InternalServerError(e.getMessage))
    }
  }
}
