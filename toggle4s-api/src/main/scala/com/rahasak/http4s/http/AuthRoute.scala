package com.rahasak.http4s.http

import cats.effect.IO
import com.rahasak.http4s.auth.TokenAuth.AuthToken
import com.rahasak.http4s.protocol.AuthRequest
import com.rahasak.http4s.repo.AuthRepo
import com.rahasak.http4s.util.{CryptoUtil, DateUtil}
import io.circe.generic.auto._
import io.circe.syntax._
import org.http4s._
import org.http4s.circe.CirceEntityCodec._
import org.http4s.dsl.Http4sDsl

object AuthRoute {
  /**
    * define following APIS
    *   1. authenticate user — POST api/v1/auth
    */
  def routes(authRepo: AuthRepo): HttpRoutes[IO] = {
    val dsl = new Http4sDsl[IO] {}
    import dsl._

    HttpRoutes.of[IO] {
      case req@POST -> Root / "auth" =>
        req.decode[AuthRequest] { r =>
          authRepo.getAuthUser(r.username, r.password)
            .flatMap {
              case Some(_) =>
                // generate bearer token for user
                val authToken = AuthToken(r.username, "", "", DateUtil.timestamp(), 60)
                val bearer = CryptoUtil.encodeBase64(authToken.asJson.noSpaces)
                val authorization = s"Bearer $bearer"

                Ok("Login success")
                  .map(_.putHeaders(Header.ToRaw.keyValuesToRaw(("Authorization", authorization))))
              case None =>
                BadRequest("Login failed")
            }
            .handleErrorWith(e => BadRequest(s"Fail authenticate user, ${e.getMessage}"))
        }.handleErrorWith(e => BadRequest(e.getMessage))
    }
  }
}

