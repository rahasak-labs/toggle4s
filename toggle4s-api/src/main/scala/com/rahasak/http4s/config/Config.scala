package com.rahasak.http4s.config

import cats.effect.IO
import io.circe.config.parser
import io.circe.generic.auto._

case class Config(serverConfig: ServerConfig, postgresConfig: PostgresConfig, ldapConfig: LdapConfig)

case class ServerConfig(port: Int, host: String)

case class PostgresConfig(host: String, port: Int, user: String, database: String, password: String)

case class LdapConfig(host: String, dc1: String, dc2: String)

object Config {
  def load(): IO[Config] = {
    for {
      serverConf <- parser.decodePathF[IO, ServerConfig]("server")
      postgresConf <- parser.decodePathF[IO, PostgresConfig]("postgres")
      ldapConf <- parser.decodePathF[IO, LdapConfig]("ldap")
    } yield Config(serverConf, postgresConf, ldapConf)
  }
}
