name := "toggle4s-api"

version := "1.0"

scalaVersion := "2.13.4"

libraryDependencies ++= {

  lazy val http4sVersion = "0.23.2"
  lazy val circeVersion = "0.14.0"
  lazy val circeConfigVersion = "0.7.0"
  lazy val circeGenericVersion = "0.14.1"
  lazy val enumeratumCirceVersion = "1.6.0"

  Seq(
    "org.http4s"            %% "http4s-blaze-server"    % http4sVersion,
    "org.http4s"            %% "http4s-circe"           % http4sVersion,
    "org.http4s"            %% "http4s-dsl"             % http4sVersion,
    "io.circe"              %% "circe-core"             % circeVersion,
    "io.circe"              %% "circe-generic"          % circeVersion,
    "io.circe"              %% "circe-config"           % circeConfigVersion,
    "io.circe"              %% "circe-generic-extras"   % circeGenericVersion,
    "com.beachape"          %% "enumeratum-circe"       % enumeratumCirceVersion,
    "org.tpolecat"          %% "skunk-core"             % "0.2.2",
    "org.slf4j"             % "slf4j-api"               % "1.7.5",
    "ch.qos.logback"        % "logback-classic"         % "1.0.9"
  )

}

addCompilerPlugin("org.typelevel" % "kind-projector" % "0.13.2" cross CrossVersion.full)

assemblyMergeStrategy in assembly := {
  case PathList(ps @ _*) if ps.last endsWith ".properties" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith ".RSA" => MergeStrategy.discard
  case PathList(ps @ _*) if ps.last endsWith ".keys" => MergeStrategy.discard
  case PathList(ps @ _*) if ps.last endsWith ".logs" => MergeStrategy.discard
  case "module-info.class" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}