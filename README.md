# toggle4s

toggle4s is the feature toggle service for microservices. It stores all the features 
toggles in Etcd storage and facilitates to dynamically configure the toggles via 
HTTP REST api. There is a web app which built on top of REST API to facilitate the 
feature toggle, create/edit/delete/search functions. The service mainly built with 
using `http4s` functional HTTP library, `circe` JSON library and `cat-effects` IO monad 
stack. read more about toggl4s from [here](https://medium.com/@itseranga/toggl4s-feature-toggle-service-with-etcd-backend-77e146e0fc32).
